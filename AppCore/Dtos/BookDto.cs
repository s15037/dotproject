﻿namespace AppCore.Dtos;

public class BookDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Author { get; set; }
    public int LibraryId { get; set; }
    //public Library Library { get; set; }
}