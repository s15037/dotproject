﻿using AppCore.Dtos;
using Infr.Model;
using Infr.Repositories;


namespace AppCore.Services;

public class BookService : IBookService
{
    private readonly IBookRepository _bookRepository;

    public BookService(IBookRepository bookRepository)
    {
        _bookRepository = bookRepository;
    }

    public async Task<BookDto> GetBookById(int id)
    {
        var book = await _bookRepository.GetBookById(id);
        var result = new BookDto
        {
            Id = book.Id,
            Name = book.Name,
            Author = book.Author,
            LibraryId = book.LibraryId
        };
        return result;
    }

    public async Task<List<BookDto>> GetBooksByName(string name)
    {
        var books = await _bookRepository.GetBooksByName(name);
        var booksDTO = new List<BookDto>();

        foreach (var book in books)
        {
            var bookDTO = new BookDto
            {
                Id = book.Id,
                Author = book.Author,
                Name = book.Name,
                LibraryId = book.LibraryId
            };
            booksDTO.Add(bookDTO);
        }

        return booksDTO;
    }


    public async Task<List<BookDto>> GetBooks()
    {
        var booksDTO = new List<BookDto>();
        var books = await _bookRepository.GetBooks();

        foreach (var book in books)
        {
            var bookDTO = new BookDto
            {
                Id = book.Id,
                Name = book.Name,
                Author = book.Author,
                LibraryId = book.LibraryId
            };
            booksDTO.Add(bookDTO);
        }

        return booksDTO;
    }

    public async Task<BookDto> AddBook(Book book)
    {
        var addedBook = await _bookRepository.AddBook(book);
        var bookDTO = new BookDto
        {
            Id = addedBook.Id,
            Name = addedBook.Name,
            Author = addedBook.Author,
            LibraryId = addedBook.LibraryId
        };
        return bookDTO;
    }

    public async Task<BookDto> UpdateBook(Book book)
    {
        var updatedBook = await _bookRepository.UpdateBook(book);
        var bookDTO = new BookDto
        {
            Id = updatedBook.Id,
            Name = updatedBook.Name,
            Author = updatedBook.Author,
            LibraryId = updatedBook.LibraryId
        };
        return bookDTO;
    }

    public async Task DeleteBook(Book book)
    {
        await _bookRepository.DeleteBook(book);
    }
}