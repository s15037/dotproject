﻿using AppCore.Dtos;
using Infr.Model;


namespace AppCore.Services;

public interface IBookService
{
    Task<BookDto> GetBookById(int id);
    Task<List<BookDto>> GetBooks();
    Task<BookDto> AddBook(Book book);
    Task DeleteBook(Book book);
    Task<List<BookDto>> GetBooksByName(string name);
}