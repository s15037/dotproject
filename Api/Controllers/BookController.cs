﻿using AppCore.Dtos;
using AppCore.Services;
using Infr.Model;
using Infr.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("[controller]")]
public class BookController : ControllerBase
{
    private readonly IBookService _bookService;
    private readonly IBookRepository _bookRepository;

    public BookController(IBookService _bookService, IBookRepository _bookRepository)
    {
        this._bookService = _bookService;
        this._bookRepository = _bookRepository;
    }

    [HttpGet("GetAllBooks")]
    public async Task<IActionResult> GetAllBooks()
    {
        var booksList = await _bookService.GetBooks();
        return Ok(booksList);
    }

    [HttpGet("GetBookById/{id}")]
    public async Task<IActionResult> GetBookById(int id)
    {
        var book = _bookService.GetBookById(id);
        return Ok(book);
    }

    [HttpPost("GetBooksById")]
    public async Task<IActionResult> GetBooksById([FromBody] string name)
    {
        var books = await _bookService.GetBooksByName(name);
        return Ok(books);
    }

    [HttpPut("AddBook")]
    public async Task<IActionResult> AddBook([FromBody] Book book)
    {
        var addedBook = await _bookService.AddBook(book);
        return Ok(addedBook);
    }
    

}