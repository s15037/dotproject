﻿// using Microsoft.EntityFrameworkCore;
// using Microsoft.Extensions.Configuration;
// using Microsoft.Extensions.DependencyInjection;
//
// namespace Infrastructure.DAL;
//
// public static class DbExtensions
// {
//     public static IServiceCollection AddDalServices(this IServiceCollection services, IConfiguration configuration)
//     {
//  
//         services.AddDbContext<MyDbContext>(x =>
//             x.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
//         return services;
//     }
// }