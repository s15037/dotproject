﻿using Infr.Model;
using Microsoft.EntityFrameworkCore;

namespace Infr.DAL;

public class MyDemoDbContext : DbContext
{
    public MyDemoDbContext(DbContextOptions options) : base(options)
    {
    
    }
    
    public DbSet<Library> Library { get; set; }
    public DbSet<Book> Book { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
}