﻿using Infr.DAL;
using Infr.Model;
using Microsoft.EntityFrameworkCore;

namespace Infr.Repositories;

public class BookRepository : IBookRepository
{
    private readonly MyDemoDbContext _context;

    public BookRepository(MyDemoDbContext context)
    {
        _context = context;
    }

    public async Task<Book> GetBookById(int id)
    {
        var book = await _context.Book.Where(x => x.Id == id).FirstOrDefaultAsync();
        return book ?? throw new Exception("Book nie istnieje");
    }

    public async Task<List<Book>> GetBooksByName(string name)
    {
        var books = await _context.Book.Where(x => x.Name.Contains(name)).ToListAsync();
        return books;
    }

    public async Task<List<Book>> GetBooks()
    {
        var books = await _context.Book.ToListAsync();
        return books;
    }

    public async Task<Book> AddBook(Book book)
    {
        await _context.Book.AddAsync(book);
        await _context.SaveChangesAsync();
        return book;
    }

    public async Task<Book> UpdateBook(Book book)
    {
        var dbBook = await _context.Book.Where(x => x.Id == book.Id).FirstOrDefaultAsync();

        if (dbBook is null)
        {
            throw new Exception("Ksiazka nie istnieje");
        }

        dbBook.Author = book.Author;
        dbBook.Name = book.Name;


        _context.Book.Update(book);
        await _context.SaveChangesAsync();

        return book;
    }

    public async Task DeleteBook(Book book)
    {
        var dbBook = await _context.Book.Where(x => x.Id == book.Id).FirstOrDefaultAsync();
        if (dbBook is null)
        {
            throw new Exception("Ksiazka nie istnieje");
        }

        _context.Book.Remove(book);
        await _context.SaveChangesAsync();
    }
}