﻿using Infr.Model;

namespace Infr.Repositories;

public interface IBookRepository
{
    Task<Book> GetBookById(int id);
    Task<List<Book>> GetBooks();
    Task<Book> AddBook(Book book);
    Task<Book> UpdateBook(Book book);
    Task<List<Book>> GetBooksByName(string name);
    Task DeleteBook(Book book);
}